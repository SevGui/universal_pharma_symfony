<?php

namespace universalPharma\traitementBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UtilisateurType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('adressutilisateur', TextType::class)
                ->add('cputilisateur', IntegerType::class)
                ->add('villutilisateur', TextType::class)
                ->add('Modifier', SubmitType::class)
                ->add('Retour', SubmitType::class)
                ->add('MiseaJour', SubmitType::class)
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'universalPharma\traitementBundle\Entity\Utilisateur'
        ));
    }

}
