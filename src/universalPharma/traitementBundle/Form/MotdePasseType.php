<?php

namespace universalPharma\traitementBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MotdePasseType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('mdputilisateur', PasswordType::class, 
                        array('required' => true, 'attr' => array('placeholder' => 'votre ancien mot de passe')))
//                ->add('nouveaumdputilisateur', PasswordType::class, 
//                        array('required' => true,'mapped' => false, 'attr' => array('placeholder' => 'votre nouveau mot de passe')))
                ->add('nouveaumdputilisateur', RepeatedType::class, 
                        array('type' => PasswordType::class, 
                            'invalid_message' => 'Les mots de passe entrés ne correspondent pas.',
                            'required' => true, 
                            'mapped' => false,
                            'options' => array('attr' => array('placeholder' => 'votre nouveau mot de passe')),))
                ->add('Modifier', SubmitType::class)
                ->add('Retour', SubmitType::class)
        ;
    }
//$builder->add('password', RepeatedType::class, array(
//    'type' => PasswordType::class,
//    'invalid_message' => 'The password fields must match.',
//    'options' => array('attr' => array('class' => 'password-field')),
//    'required' => true,
//    'first_options'  => array('label' => 'Password'),
//    'second_options' => array('label' => 'Repeat Password'),
//));

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'universalPharma\traitementBundle\Entity\Utilisateur'
        ));
    }

}
