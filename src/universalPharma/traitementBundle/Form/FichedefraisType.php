<?php

namespace universalPharma\traitementBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use universalPharma\traitementBundle\Entity\Utilisateur;

//use ComptableBundle\Entity\Etatfiche;

class FichedefraisType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('utilisateur', EntityType::class, [
                    'class' => 'universalPharmatraitementBundle:Utilisateur',
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('u')
                                ->where('u.idroles = :idroles')
                                ->setParameter('idroles', 'V')
                                ->orderBy('u.nomutilisateur', 'ASC');
                    },
                    'choice_label' => 'nomprenommatricule',
                    'multiple' => false,
                    'expanded' => false,
                    'placeholder' => '- Choisir un consultant -',
                    'required' => true
                ])
                ->add('etatfrais', EntityType::class, [
                    'class' => 'universalPharmatraitementBundle:Statusdesfrais',
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('u');
//                                ->where("u.idstatfrais<>:creee")
//                                ->andWhere("u.idstatfrais<>:remboursee")
//                                ->setParameters(['creee' => Etatfiche::CREE, 'remboursee' => Etatfiche::REMBOURSEE]);
                    },
                    'choice_label' => 'libstatfrais',
                    'multiple' => false,
                    'expanded' => false,
                    'placeholder' => '- Choisir un état -',
                    'required' => true
                ])
                ->add('rechercher', SubmitType::class, array('label' => 'Rechercher'))
        ;
    }

}
