<?php

namespace universalPharma\traitementBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Roles
 *
 * @ORM\Table(name="roles")
 * @ORM\Entity(repositoryClass="universalPharma\traitementBundle\Repository\RolesRepository")
 */
class Roles
{
    /**
     * @var string
     *
     * @ORM\Column(name="idroles", type="string", length=2, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idroles;

    /**
     * @var string
     *
     * @ORM\Column(name="libRoles", type="string", length=30, nullable=true)
     */
    private $libroles;



    /**
     * Get idroles
     *
     * @return string
     */
    public function getIdroles()
    {
        return $this->idroles;
    }

    /**
     * Set libroles
     *
     * @param string $libroles
     *
     * @return Roles
     */
    public function setLibroles($libroles)
    {
        $this->libroles = $libroles;

        return $this;
    }

    /**
     * Get libroles
     *
     * @return string
     */
    public function getLibroles()
    {
        return $this->libroles;
    }
}
