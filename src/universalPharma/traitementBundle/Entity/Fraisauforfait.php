<?php

namespace universalPharma\traitementBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Fraisauforfait
 *
 * @ORM\Table(name="fraisauforfait")
 * @ORM\Entity
 */
class Fraisauforfait
{
    /**
     * @var string
     *
     * @ORM\Column(name="idFAF", type="string", length=3, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idfaf;

    /**
     * @var string
     *
     * @ORM\Column(name="libFAF", type="string", length=30, nullable=true)
     */
    private $libfaf;

    /**
     * @var string
     *
     * @ORM\Column(name="montantFAF", type="decimal", precision=4, scale=0, nullable=true)
     */
    private $montantfaf;



    /**
     * Get idfaf
     *
     * @return string
     */
    public function getIdfaf()
    {
        return $this->idfaf;
    }

    /**
     * Set libfaf
     *
     * @param string $libfaf
     *
     * @return Fraisauforfait
     */
    public function setLibfaf($libfaf)
    {
        $this->libfaf = $libfaf;

        return $this;
    }

    /**
     * Get libfaf
     *
     * @return string
     */
    public function getLibfaf()
    {
        return $this->libfaf;
    }

    /**
     * Set montantfaf
     *
     * @param string $montantfaf
     *
     * @return Fraisauforfait
     */
    public function setMontantfaf($montantfaf)
    {
        $this->montantfaf = $montantfaf;

        return $this;
    }

    /**
     * Get montantfaf
     *
     * @return string
     */
    public function getMontantfaf()
    {
        return $this->montantfaf;
    }
}
