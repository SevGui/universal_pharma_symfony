<?php

namespace universalPharma\traitementBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Statusdesfrais
 *
 * @ORM\Table(name="statusdesfrais")
 * @ORM\Entity
 */
class Statusdesfrais
{
    /**
     * @var string
     *
     * @ORM\Column(name="idStatFrais", type="string", length=2, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idstatfrais;

    /**
     * @var string
     *
     * @ORM\Column(name="libStatFrais", type="string", length=30, nullable=true)
     */
    private $libstatfrais;



    /**
     * Get idstatfrais
     *
     * @return string
     */
    public function getIdstatfrais()
    {
        return $this->idstatfrais;
    }

    /**
     * Set libstatfrais
     *
     * @param string $libstatfrais
     *
     * @return Statusdesfrais
     */
    public function setLibstatfrais($libstatfrais)
    {
        $this->libstatfrais = $libstatfrais;

        return $this;
    }

    /**
     * Get libstatfrais
     *
     * @return string
     */
    public function getLibstatfrais()
    {
        return $this->libstatfrais;
    }
}
