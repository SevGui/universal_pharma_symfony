<?php

namespace universalPharma\traitementBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Logfaf
 *
 * @ORM\Table(name="logfaf")
 * @ORM\Entity
 */
class Logfaf
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idlogfaf", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idlogfaf;

    /**
     * @var string
     *
     * @ORM\Column(name="idutilisateur", type="string", length=5, nullable=false)
     */
    private $idutilisateur;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=30, nullable=false)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=30, nullable=false)
     */
    private $prenom;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateTrans", type="date", nullable=false)
     */
    private $datetrans;

    /**
     * @var string
     *
     * @ORM\Column(name="anneemois", type="string", length=6, nullable=false)
     */
    private $anneemois;

    /**
     * @var string
     *
     * @ORM\Column(name="idfaf", type="string", length=3, nullable=false)
     */
    private $idfaf;

    /**
     * @var integer
     *
     * @ORM\Column(name="quantite", type="integer", nullable=false)
     */
    private $quantite;

    /**
     * @var string
     *
     * @ORM\Column(name="transactionFAF", type="string", length=6, nullable=false)
     */
    private $transactionfaf;



    /**
     * Get idlogfaf
     *
     * @return integer
     */
    public function getIdlogfaf()
    {
        return $this->idlogfaf;
    }

    /**
     * Set idutilisateur
     *
     * @param string $idutilisateur
     *
     * @return Logfaf
     */
    public function setIdutilisateur($idutilisateur)
    {
        $this->idutilisateur = $idutilisateur;

        return $this;
    }

    /**
     * Get idutilisateur
     *
     * @return string
     */
    public function getIdutilisateur()
    {
        return $this->idutilisateur;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Logfaf
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     *
     * @return Logfaf
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set datetrans
     *
     * @param \DateTime $datetrans
     *
     * @return Logfaf
     */
    public function setDatetrans($datetrans)
    {
        $this->datetrans = $datetrans;

        return $this;
    }

    /**
     * Get datetrans
     *
     * @return \DateTime
     */
    public function getDatetrans()
    {
        return $this->datetrans;
    }

    /**
     * Set anneemois
     *
     * @param string $anneemois
     *
     * @return Logfaf
     */
    public function setAnneemois($anneemois)
    {
        $this->anneemois = $anneemois;

        return $this;
    }

    /**
     * Get anneemois
     *
     * @return string
     */
    public function getAnneemois()
    {
        return $this->anneemois;
    }

    /**
     * Set idfaf
     *
     * @param string $idfaf
     *
     * @return Logfaf
     */
    public function setIdfaf($idfaf)
    {
        $this->idfaf = $idfaf;

        return $this;
    }

    /**
     * Get idfaf
     *
     * @return string
     */
    public function getIdfaf()
    {
        return $this->idfaf;
    }

    /**
     * Set quantite
     *
     * @param integer $quantite
     *
     * @return Logfaf
     */
    public function setQuantite($quantite)
    {
        $this->quantite = $quantite;

        return $this;
    }

    /**
     * Get quantite
     *
     * @return integer
     */
    public function getQuantite()
    {
        return $this->quantite;
    }

    /**
     * Set transactionfaf
     *
     * @param string $transactionfaf
     *
     * @return Logfaf
     */
    public function setTransactionfaf($transactionfaf)
    {
        $this->transactionfaf = $transactionfaf;

        return $this;
    }

    /**
     * Get transactionfaf
     *
     * @return string
     */
    public function getTransactionfaf()
    {
        return $this->transactionfaf;
    }
}
