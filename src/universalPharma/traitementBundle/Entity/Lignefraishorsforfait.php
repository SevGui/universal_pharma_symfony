<?php

namespace universalPharma\traitementBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Lignefraishorsforfait
 *
 * @ORM\Table(name="lignefraishorsforfait", indexes={@ORM\Index(name="fk_idTVA", columns={"idTVA"}), @ORM\Index(name="fk_idUtilisateur", columns={"idUtilisateur"})})
 * @ORM\Entity
 */
class Lignefraishorsforfait
{
    /**
     * @var integer
     *
     * @ORM\Column(name="idLigneFHF", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idlignefhf;

    /**
     * @var string
     *
     * @ORM\Column(name="anneeMoisLigneFHF", type="string", length=10, nullable=true)
     */
    private $anneemoislignefhf;

    /**
     * @var string
     *
     * @ORM\Column(name="libLigneFHF", type="string", length=50, nullable=true)
     */
    private $liblignefhf;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateJustiFHF", type="date", nullable=true)
     */
    private $datejustifhf;

    /**
     * @var string
     *
     * @ORM\Column(name="montLigneFHF", type="decimal", precision=5, scale=0, nullable=true)
     */
    private $montlignefhf;

    /**
     * @var string
     *
     * @ORM\Column(name="scanJustiFHF", type="string", length=100, nullable=true)
     */
    private $scanjustifhf;

    /**
     * @var \Tva
     *
     * @ORM\ManyToOne(targetEntity="Tva")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idTVA", referencedColumnName="idTVA")
     * })
     */
    private $idtva;

    /**
     * @var \Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idUtilisateur", referencedColumnName="idUtilisateur")
     * })
     */
    private $idutilisateur;



    /**
     * Get idlignefhf
     *
     * @return integer
     */
    public function getIdlignefhf()
    {
        return $this->idlignefhf;
    }

    /**
     * Set anneemoislignefhf
     *
     * @param string $anneemoislignefhf
     *
     * @return Lignefraishorsforfait
     */
    public function setAnneemoislignefhf($anneemoislignefhf)
    {
        $this->anneemoislignefhf = $anneemoislignefhf;

        return $this;
    }

    /**
     * Get anneemoislignefhf
     *
     * @return string
     */
    public function getAnneemoislignefhf()
    {
        return $this->anneemoislignefhf;
    }

    /**
     * Set liblignefhf
     *
     * @param string $liblignefhf
     *
     * @return Lignefraishorsforfait
     */
    public function setLiblignefhf($liblignefhf)
    {
        $this->liblignefhf = $liblignefhf;

        return $this;
    }

    /**
     * Get liblignefhf
     *
     * @return string
     */
    public function getLiblignefhf()
    {
        return $this->liblignefhf;
    }

    /**
     * Set datejustifhf
     *
     * @param \DateTime $datejustifhf
     *
     * @return Lignefraishorsforfait
     */
    public function setDatejustifhf($datejustifhf)
    {
        $this->datejustifhf = $datejustifhf;

        return $this;
    }

    /**
     * Get datejustifhf
     *
     * @return \DateTime
     */
    public function getDatejustifhf()
    {
        return $this->datejustifhf;
    }

    /**
     * Set montlignefhf
     *
     * @param string $montlignefhf
     *
     * @return Lignefraishorsforfait
     */
    public function setMontlignefhf($montlignefhf)
    {
        $this->montlignefhf = $montlignefhf;

        return $this;
    }

    /**
     * Get montlignefhf
     *
     * @return string
     */
    public function getMontlignefhf()
    {
        return $this->montlignefhf;
    }

    /**
     * Set scanjustifhf
     *
     * @param string $scanjustifhf
     *
     * @return Lignefraishorsforfait
     */
    public function setScanjustifhf($scanjustifhf)
    {
        $this->scanjustifhf = $scanjustifhf;

        return $this;
    }

    /**
     * Get scanjustifhf
     *
     * @return string
     */
    public function getScanjustifhf()
    {
        return $this->scanjustifhf;
    }

    /**
     * Set idtva
     *
     * @param \universalPharma\traitementBundle\Entity\Tva $idtva
     *
     * @return Lignefraishorsforfait
     */
    public function setIdtva(\universalPharma\traitementBundle\Entity\Tva $idtva = null)
    {
        $this->idtva = $idtva;

        return $this;
    }

    /**
     * Get idtva
     *
     * @return \universalPharma\traitementBundle\Entity\Tva
     */
    public function getIdtva()
    {
        return $this->idtva;
    }

    /**
     * Set idutilisateur
     *
     * @param \universalPharma\traitementBundle\Entity\Utilisateur $idutilisateur
     *
     * @return Lignefraishorsforfait
     */
    public function setIdutilisateur(\universalPharma\traitementBundle\Entity\Utilisateur $idutilisateur = null)
    {
        $this->idutilisateur = $idutilisateur;

        return $this;
    }

    /**
     * Get idutilisateur
     *
     * @return \universalPharma\traitementBundle\Entity\Utilisateur
     */
    public function getIdutilisateur()
    {
        return $this->idutilisateur;
    }
}
