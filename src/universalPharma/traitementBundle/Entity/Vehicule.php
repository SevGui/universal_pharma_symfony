<?php

namespace universalPharma\traitementBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Vehicule
 *
 * @ORM\Table(name="vehicule")
 * @ORM\Entity
 */
class Vehicule
{
    /**
     * @var string
     *
     * @ORM\Column(name="idVehicule", type="string", length=5, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idvehicule;

    /**
     * @var string
     *
     * @ORM\Column(name="typeVehicule", type="string", length=30, nullable=true)
     */
    private $typevehicule;

    /**
     * @var string
     *
     * @ORM\Column(name="indKmVehicule", type="decimal", precision=3, scale=2, nullable=true)
     */
    private $indkmvehicule;



    /**
     * Get idvehicule
     *
     * @return string
     */
    public function getIdvehicule()
    {
        return $this->idvehicule;
    }

    /**
     * Set typevehicule
     *
     * @param string $typevehicule
     *
     * @return Vehicule
     */
    public function setTypevehicule($typevehicule)
    {
        $this->typevehicule = $typevehicule;

        return $this;
    }

    /**
     * Get typevehicule
     *
     * @return string
     */
    public function getTypevehicule()
    {
        return $this->typevehicule;
    }

    /**
     * Set indkmvehicule
     *
     * @param string $indkmvehicule
     *
     * @return Vehicule
     */
    public function setIndkmvehicule($indkmvehicule)
    {
        $this->indkmvehicule = $indkmvehicule;

        return $this;
    }

    /**
     * Get indkmvehicule
     *
     * @return string
     */
    public function getIndkmvehicule()
    {
        return $this->indkmvehicule;
    }
}
