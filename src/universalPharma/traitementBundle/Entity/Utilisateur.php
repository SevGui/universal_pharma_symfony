<?php

namespace universalPharma\traitementBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Utilisateur
 *
 * @ORM\Table(name="utilisateur", uniqueConstraints={@ORM\UniqueConstraint(name="loginUtilisateur", columns={"loginUtilisateur"}), @ORM\UniqueConstraint(name="mailUtilisateur", columns={"mailUtilisateur"}), @ORM\UniqueConstraint(name="loginUtilisateur_2", columns={"loginUtilisateur"})}, indexes={@ORM\Index(name="fk_idRoles", columns={"idroles"})})
 * @ORM\Entity(repositoryClass="universalPharma\traitementBundle\Repository\UtilisateurRepository")
 */
class Utilisateur {

    /**
     * @var string
     *
     * @ORM\Column(name="idUtilisateur", type="string", length=5, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idutilisateur;

    /**
     * @var string
     *
     * @ORM\Column(name="nomUtilisateur", type="string", length=30, nullable=true)
     */
    private $nomutilisateur;

    /**
     * @var string
     *
     * @ORM\Column(name="prenomUtilisateur", type="string", length=30, nullable=true)
     */
    private $prenomutilisateur;

    /**
     * @var string
     *
     * @ORM\Column(name="loginUtilisateur", type="string", length=20, nullable=true)
     */
    private $loginutilisateur;

    /**
     * @var string
     *
     * @ORM\Column(name="mdpUtilisateur", type="string", length=40, nullable=true)
     */
    private $mdputilisateur;

    /**
     * @var string
     *
     * @ORM\Column(name="adressUtilisateur", type="string", length=50, nullable=true)
     */
    private $adressutilisateur;

    /**
     * @var string
     *
     * @ORM\Column(name="cpUtilisateur", type="decimal", precision=5, scale=0, nullable=true)
     */
    private $cputilisateur;

    /**
     * @var string
     *
     * @ORM\Column(name="villUtilisateur", type="string", length=50, nullable=true)
     */
    private $villutilisateur;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datEmbauche", type="date", nullable=true)
     */
    private $datembauche;

    /**
     * @var string
     *
     * @ORM\Column(name="civUtilisateur", type="string", length=5, nullable=true)
     */
    private $civutilisateur;

    /**
     * @var string
     *
     * @ORM\Column(name="mailUtilisateur", type="string", length=50, nullable=true)
     */
    private $mailutilisateur;

    /**
     * @var string
     *
     * @ORM\Column(name="etatUtilisateur", type="string", length=7, nullable=true)
     */
    private $etatutilisateur;

    /**
     * @var integer
     *
     * @ORM\Column(name="logAttempt", type="integer", nullable=false)
     */
    private $logattempt = '0';

    /**
     * @var \Roles
     *
     * @ORM\ManyToOne(targetEntity="Roles")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idroles", referencedColumnName="idroles")
     * })
     */
    private $idroles;

    /**
     * Get idutilisateur
     *
     * @return string
     */
    public function getIdutilisateur() {
        return $this->idutilisateur;
    }

    /**
     * Set nomutilisateur
     *
     * @param string $nomutilisateur
     *
     * @return Utilisateur
     */
    public function setNomutilisateur($nomutilisateur) {
        $this->nomutilisateur = $nomutilisateur;

        return $this;
    }

    /**
     * Get nomutilisateur
     *
     * @return string
     */
    public function getNomutilisateur() {
        return $this->nomutilisateur;
    }

    /**
     * Set prenomutilisateur
     *
     * @param string $prenomutilisateur
     *
     * @return Utilisateur
     */
    public function setPrenomutilisateur($prenomutilisateur) {
        $this->prenomutilisateur = $prenomutilisateur;

        return $this;
    }

    /**
     * Get prenomutilisateur
     *
     * @return string
     */
    public function getPrenomutilisateur() {
        return $this->prenomutilisateur;
    }

    /**
     * Set loginutilisateur
     *
     * @param string $loginutilisateur
     *
     * @return Utilisateur
     */
    public function setLoginutilisateur($loginutilisateur) {
        $this->loginutilisateur = $loginutilisateur;

        return $this;
    }

    /**
     * Get loginutilisateur
     *
     * @return string
     */
    public function getLoginutilisateur() {
        return $this->loginutilisateur;
    }

    /**
     * Set mdputilisateur
     *
     * @param string $mdputilisateur
     *
     * @return Utilisateur
     */
    public function setMdputilisateur($mdputilisateur) {
        $this->mdputilisateur = $mdputilisateur;

        return $this;
    }

    /**
     * Get mdputilisateur
     *
     * @return string
     */
    public function getMdputilisateur() {
        return $this->mdputilisateur;
    }

    /**
     * Set adressutilisateur
     *
     * @param string $adressutilisateur
     *
     * @return Utilisateur
     */
    public function setAdressutilisateur($adressutilisateur) {
        $this->adressutilisateur = $adressutilisateur;

        return $this;
    }

    /**
     * Get adressutilisateur
     *
     * @return string
     */
    public function getAdressutilisateur() {
        return $this->adressutilisateur;
    }

    /**
     * Set cputilisateur
     *
     * @param string $cputilisateur
     *
     * @return Utilisateur
     */
    public function setCputilisateur($cputilisateur) {
        $this->cputilisateur = $cputilisateur;

        return $this;
    }

    /**
     * Get cputilisateur
     *
     * @return string
     */
    public function getCputilisateur() {
        return $this->cputilisateur;
    }

    /**
     * Set villutilisateur
     *
     * @param string $villutilisateur
     *
     * @return Utilisateur
     */
    public function setVillutilisateur($villutilisateur) {
        $this->villutilisateur = $villutilisateur;

        return $this;
    }

    /**
     * Get villutilisateur
     *
     * @return string
     */
    public function getVillutilisateur() {
        return $this->villutilisateur;
    }

    /**
     * Set datembauche
     *
     * @param \DateTime $datembauche
     *
     * @return Utilisateur
     */
    public function setDatembauche($datembauche) {
        $this->datembauche = $datembauche;

        return $this;
    }

    /**
     * Get datembauche
     *
     * @return \DateTime
     */
    public function getDatembauche() {
        return $this->datembauche;
    }

    /**
     * Set civutilisateur
     *
     * @param string $civutilisateur
     *
     * @return Utilisateur
     */
    public function setCivutilisateur($civutilisateur) {
        $this->civutilisateur = $civutilisateur;

        return $this;
    }

    /**
     * Get civutilisateur
     *
     * @return string
     */
    public function getCivutilisateur() {
        return $this->civutilisateur;
    }

    /**
     * Set mailutilisateur
     *
     * @param string $mailutilisateur
     *
     * @return Utilisateur
     */
    public function setMailutilisateur($mailutilisateur) {
        $this->mailutilisateur = $mailutilisateur;

        return $this;
    }

    /**
     * Get mailutilisateur
     *
     * @return string
     */
    public function getMailutilisateur() {
        return $this->mailutilisateur;
    }

    /**
     * Set etatutilisateur
     *
     * @param string $etatutilisateur
     *
     * @return Utilisateur
     */
    public function setEtatutilisateur($etatutilisateur) {
        $this->etatutilisateur = $etatutilisateur;

        return $this;
    }

    /**
     * Get etatutilisateur
     *
     * @return string
     */
    public function getEtatutilisateur() {
        return $this->etatutilisateur;
    }

    /**
     * Set logattempt
     *
     * @param integer $logattempt
     *
     * @return Utilisateur
     */
    public function setLogattempt($logattempt) {
        $this->logattempt = $logattempt;

        return $this;
    }

    /**
     * Get logattempt
     *
     * @return integer
     */
    public function getLogattempt() {
        return $this->logattempt;
    }

    /**
     * Set idroles
     *
     * @param \universalPharma\traitementBundle\Entity\Roles $idroles
     *
     * @return Utilisateur
     */
    public function setIdroles(\universalPharma\traitementBundle\Entity\Roles $idroles = null) {
        $this->idroles = $idroles;

        return $this;
    }

    /**
     * Get idroles
     *
     * @return \universalPharma\traitementBundle\Entity\Roles
     */
    public function getIdroles() {
        return $this->idroles;
    }

    /**
     * Get nomprenommatricule
     *
     * @return string
     */
    public function getNomprenommatricule() {
        $nomPrenomMatricule = $this->nomutilisateur . " " . $this->prenomutilisateur . " " . $this->idutilisateur;
        return $nomPrenomMatricule;
    }

}
