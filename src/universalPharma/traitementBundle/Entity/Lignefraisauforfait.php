<?php

namespace universalPharma\traitementBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Lignefraisauforfait
 *
 * @ORM\Table(name="lignefraisauforfait", indexes={@ORM\Index(name="fk_idTVA", columns={"idTVA"}), @ORM\Index(name="fk_idUtilisateur", columns={"idUtilisateur"}), @ORM\Index(name="fk_idFAF", columns={"idFAF"})})
 * @ORM\Entity
 */
class Lignefraisauforfait
{
    /**
     * @var string
     *
     * @ORM\Column(name="anneeMoisLigneFAF", type="string", length=10, nullable=true)
     */
    private $anneemoislignefaf;

    /**
     * @var string
     *
     * @ORM\Column(name="quantLigneFAF", type="decimal", precision=5, scale=0, nullable=true)
     */
    private $quantlignefaf;

    /**
     * @var integer
     *
     * @ORM\Column(name="idligneFAF", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idlignefaf;

    /**
     * @var \Fraisauforfait
     *
     * @ORM\ManyToOne(targetEntity="Fraisauforfait")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idFAF", referencedColumnName="idFAF")
     * })
     */
    private $idfaf;

    /**
     * @var \Tva
     *
     * @ORM\ManyToOne(targetEntity="Tva")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idTVA", referencedColumnName="idTVA")
     * })
     */
    private $idtva;

    /**
     * @var \Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idUtilisateur", referencedColumnName="idUtilisateur")
     * })
     */
    private $idutilisateur;



    /**
     * Set anneemoislignefaf
     *
     * @param string $anneemoislignefaf
     *
     * @return Lignefraisauforfait
     */
    public function setAnneemoislignefaf($anneemoislignefaf)
    {
        $this->anneemoislignefaf = $anneemoislignefaf;

        return $this;
    }

    /**
     * Get anneemoislignefaf
     *
     * @return string
     */
    public function getAnneemoislignefaf()
    {
        return $this->anneemoislignefaf;
    }

    /**
     * Set quantlignefaf
     *
     * @param string $quantlignefaf
     *
     * @return Lignefraisauforfait
     */
    public function setQuantlignefaf($quantlignefaf)
    {
        $this->quantlignefaf = $quantlignefaf;

        return $this;
    }

    /**
     * Get quantlignefaf
     *
     * @return string
     */
    public function getQuantlignefaf()
    {
        return $this->quantlignefaf;
    }

    /**
     * Get idlignefaf
     *
     * @return integer
     */
    public function getIdlignefaf()
    {
        return $this->idlignefaf;
    }

    /**
     * Set idfaf
     *
     * @param \universalPharma\traitementBundle\Entity\Fraisauforfait $idfaf
     *
     * @return Lignefraisauforfait
     */
    public function setIdfaf(\universalPharma\traitementBundle\Entity\Fraisauforfait $idfaf = null)
    {
        $this->idfaf = $idfaf;

        return $this;
    }

    /**
     * Get idfaf
     *
     * @return \universalPharma\traitementBundle\Entity\Fraisauforfait
     */
    public function getIdfaf()
    {
        return $this->idfaf;
    }

    /**
     * Set idtva
     *
     * @param \universalPharma\traitementBundle\Entity\Tva $idtva
     *
     * @return Lignefraisauforfait
     */
    public function setIdtva(\universalPharma\traitementBundle\Entity\Tva $idtva = null)
    {
        $this->idtva = $idtva;

        return $this;
    }

    /**
     * Get idtva
     *
     * @return \universalPharma\traitementBundle\Entity\Tva
     */
    public function getIdtva()
    {
        return $this->idtva;
    }

    /**
     * Set idutilisateur
     *
     * @param \universalPharma\traitementBundle\Entity\Utilisateur $idutilisateur
     *
     * @return Lignefraisauforfait
     */
    public function setIdutilisateur(\universalPharma\traitementBundle\Entity\Utilisateur $idutilisateur = null)
    {
        $this->idutilisateur = $idutilisateur;

        return $this;
    }

    /**
     * Get idutilisateur
     *
     * @return \universalPharma\traitementBundle\Entity\Utilisateur
     */
    public function getIdutilisateur()
    {
        return $this->idutilisateur;
    }
}
