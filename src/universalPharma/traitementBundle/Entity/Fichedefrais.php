<?php

namespace universalPharma\traitementBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Fichedefrais
 *
 * @ORM\Table(name="fichedefrais", indexes={@ORM\Index(name="fk_idVehicule", columns={"idVehicule"}), @ORM\Index(name="fk_idStatFrais", columns={"idStatFrais"}), @ORM\Index(name="fk_idUtilisateur", columns={"idUtilisateur"})})
 * @ORM\Entity(repositoryClass="universalPharma\traitementBundle\Repository\FichedefraisRepository")
 */
class Fichedefrais
{
    /**
     * @var string
     *
     * @ORM\Column(name="anneeMoisLigneFF", type="string", length=10, nullable=true)
     */
    private $anneemoisligneff;

    /**
     * @var string
     *
     * @ORM\Column(name="nbJustifFF", type="decimal", precision=2, scale=0, nullable=true)
     */
    private $nbjustifff;

    /**
     * @var string
     *
     * @ORM\Column(name="montantFF", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $montantff;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateModifFF", type="date", nullable=true)
     */
    private $datemodifff;

    /**
     * @var integer
     *
     * @ORM\Column(name="idFF", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idff;

    /**
     * @var \Statusdesfrais
     *
     * @ORM\ManyToOne(targetEntity="Statusdesfrais")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idStatFrais", referencedColumnName="idStatFrais")
     * })
     */
    private $idstatfrais;

    /**
     * @var \Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idUtilisateur", referencedColumnName="idUtilisateur")
     * })
     */
    private $idutilisateur;

    /**
     * @var \Vehicule
     *
     * @ORM\ManyToOne(targetEntity="Vehicule")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idVehicule", referencedColumnName="idVehicule")
     * })
     */
    private $idvehicule;



    /**
     * Set anneemoisligneff
     *
     * @param string $anneemoisligneff
     *
     * @return Fichedefrais
     */
    public function setAnneemoisligneff($anneemoisligneff)
    {
        $this->anneemoisligneff = $anneemoisligneff;

        return $this;
    }

    /**
     * Get anneemoisligneff
     *
     * @return string
     */
    public function getAnneemoisligneff()
    {
        return $this->anneemoisligneff;
    }

    /**
     * Set nbjustifff
     *
     * @param string $nbjustifff
     *
     * @return Fichedefrais
     */
    public function setNbjustifff($nbjustifff)
    {
        $this->nbjustifff = $nbjustifff;

        return $this;
    }

    /**
     * Get nbjustifff
     *
     * @return string
     */
    public function getNbjustifff()
    {
        return $this->nbjustifff;
    }

    /**
     * Set montantff
     *
     * @param string $montantff
     *
     * @return Fichedefrais
     */
    public function setMontantff($montantff)
    {
        $this->montantff = $montantff;

        return $this;
    }

    /**
     * Get montantff
     *
     * @return string
     */
    public function getMontantff()
    {
        return $this->montantff;
    }

    /**
     * Set datemodifff
     *
     * @param \DateTime $datemodifff
     *
     * @return Fichedefrais
     */
    public function setDatemodifff($datemodifff)
    {
        $this->datemodifff = $datemodifff;

        return $this;
    }

    /**
     * Get datemodifff
     *
     * @return \DateTime
     */
    public function getDatemodifff()
    {
        return $this->datemodifff;
    }

    /**
     * Get idff
     *
     * @return integer
     */
    public function getIdff()
    {
        return $this->idff;
    }

    /**
     * Set idstatfrais
     *
     * @param \universalPharma\traitementBundle\Entity\Statusdesfrais $idstatfrais
     *
     * @return Fichedefrais
     */
    public function setIdstatfrais(\universalPharma\traitementBundle\Entity\Statusdesfrais $idstatfrais = null)
    {
        $this->idstatfrais = $idstatfrais;

        return $this;
    }

    /**
     * Get idstatfrais
     *
     * @return \universalPharma\traitementBundle\Entity\Statusdesfrais
     */
    public function getIdstatfrais()
    {
        return $this->idstatfrais;
    }

    /**
     * Set idutilisateur
     *
     * @param \universalPharma\traitementBundle\Entity\Utilisateur $idutilisateur
     *
     * @return Fichedefrais
     */
    public function setIdutilisateur(\universalPharma\traitementBundle\Entity\Utilisateur $idutilisateur = null)
    {
        $this->idutilisateur = $idutilisateur;

        return $this;
    }

    /**
     * Get idutilisateur
     *
     * @return \universalPharma\traitementBundle\Entity\Utilisateur
     */
    public function getIdutilisateur()
    {
        return $this->idutilisateur;
    }

    /**
     * Set idvehicule
     *
     * @param \universalPharma\traitementBundle\Entity\Vehicule $idvehicule
     *
     * @return Fichedefrais
     */
    public function setIdvehicule(\universalPharma\traitementBundle\Entity\Vehicule $idvehicule = null)
    {
        $this->idvehicule = $idvehicule;

        return $this;
    }

    /**
     * Get idvehicule
     *
     * @return \universalPharma\traitementBundle\Entity\Vehicule
     */
    public function getIdvehicule()
    {
        return $this->idvehicule;
    }
}
