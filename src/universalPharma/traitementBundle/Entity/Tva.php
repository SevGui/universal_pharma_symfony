<?php

namespace universalPharma\traitementBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tva
 *
 * @ORM\Table(name="tva")
 * @ORM\Entity
 */
class Tva
{
    /**
     * @var string
     *
     * @ORM\Column(name="idTVA", type="decimal", precision=2, scale=0, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idtva;

    /**
     * @var string
     *
     * @ORM\Column(name="produitTVA", type="decimal", precision=2, scale=0, nullable=true)
     */
    private $produittva;

    /**
     * @var string
     *
     * @ORM\Column(name="tauxTVA", type="string", length=30, nullable=true)
     */
    private $tauxtva;



    /**
     * Get idtva
     *
     * @return string
     */
    public function getIdtva()
    {
        return $this->idtva;
    }

    /**
     * Set produittva
     *
     * @param string $produittva
     *
     * @return Tva
     */
    public function setProduittva($produittva)
    {
        $this->produittva = $produittva;

        return $this;
    }

    /**
     * Get produittva
     *
     * @return string
     */
    public function getProduittva()
    {
        return $this->produittva;
    }

    /**
     * Set tauxtva
     *
     * @param string $tauxtva
     *
     * @return Tva
     */
    public function setTauxtva($tauxtva)
    {
        $this->tauxtva = $tauxtva;

        return $this;
    }

    /**
     * Get tauxtva
     *
     * @return string
     */
    public function getTauxtva()
    {
        return $this->tauxtva;
    }
}
