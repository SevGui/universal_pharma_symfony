<?php

namespace universalPharma\traitementBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use universalPharma\traitementBundle\Entity\Utilisateur;
use universalPharma\traitementBundle\Entity\Fichedefrais;
use universalPharma\traitementBundle\Entity\Statusdesfrais;
use universalPharma\traitementBundle\Form\ConnexionType;
use universalPharma\traitementBundle\Form\UtilisateurType;
use universalPharma\traitementBundle\Form\MotdePasseType;
use universalPharma\traitementBundle\Form\FichedefraisType;
use universalPharma\traitementBundle\Form\ActionFichedefraisType;
use Symfony\Component\HttpFoundation\Request;

/**
 * Description of fraisController
 *
 * @author Severine
 */
class fraisController extends Controller {
    /*
     * Controlleur qui gère la page d'accueil pour la connexion du comptable
     */

    public function indexAction(Request $request) {
        //on crée un utilisateur
        $user = new Utilisateur();

        //on récupère le formulaire
        //Le fait que cela soit souligné, c'est netbeans qui n'est pas à jour
        //sur Symfony
        // Edit : régler dans la configuration de netbeans
        $formConnexion = $this->createForm(ConnexionType::class, $user);

        //on génère le html du formulaire crée
        $formConnexionView = $formConnexion->createView();

        //Si la requete est en POST
        if ($request->isMethod('POST')) {
            $formConnexion->handleRequest($request);

            $login = $formConnexion['loginutilisateur']->getData();
            $mdp = sha1($formConnexion['mdputilisateur']->getData());

//            dump($mdp);
//            exit();
            $repository = $this->getDoctrine()->getManager()->getRepository('universalPharmatraitementBundle:Utilisateur');

            $session = $request->getSession();
            $user = $repository->findOneByloginutilisateur($login);
//            dump($user);
//            exit();

            if ($user == null) {
                $session->getFlashBag()->add('errMsg', 'Login ou mot de passe incorrect');
                return $this->render('universalPharmatraitementBundle:frais:index.html.twig', array('formconnexion' => $formConnexionView));
            }

            $bddPwd = $user->getmdpUtilisateur();
            $bddLogin = $user->getloginUtilisateur();
            $bddRole = $user->getIdroles()->getIdroles();
//            dump($bddrole);
//            exit();

            if ($login == $bddLogin && $mdp == $bddPwd && $bddRole == "C") {
//                 echo'equals';
//                  $this->get('session')->setFlash('succesMsg', 'Vous êtes bien connecté');

                $session->set('id', $user->getIdutilisateur());
                $session->set('nom', $user->getnomUtilisateur());
                $session->set('role', $user->getidroles()->getLibroles());

//                $session->getFlashBag()->add('succesMsg', 'Vous êtes bien connecté');
                //on redirige vers la page du tableau de bord
                return $this->redirectToRoute('universal_pharmatraitement_dashboard');
            } elseif ($bddRole == "V") {
                $session->getFlashBag()->add('errMsg', 'Vous êtes consultant. Ceci est une zone comptable');
                return $this->render('universalPharmatraitementBundle:frais:index.html.twig', array('formconnexion' => $formConnexionView));
            } else {
                $session->getFlashBag()->add('errMsg', 'Login ou mot de passe incorrect');
                return $this->render('universalPharmatraitementBundle:frais:index.html.twig', array('formconnexion' => $formConnexionView));
            }
        }

        return $this->render('universalPharmatraitementBundle:frais:index.html.twig', array('formconnexion' => $formConnexionView));
    }

    /*
     * Controlleur qui permet d'afficher la session dans le tableau de bord
     */

    public function dashboardAction(Request $request) {

        $session = $request->getSession();
        $nom = $session->get('nom');
        $role = $session->get('role');

        return $this->render('universalPharmatraitementBundle:frais:dashboard.html.twig', array('nom' => $nom, 'role' => $role));
    }

    /*
     * Controlleur qui permet de voir et d'enregistrer dans bdd les informations
     * de l'utilisateur
     */

    public function cpuserAction(Request $request) {

        $session = $request->getSession();
        $nom = $session->get('nom');
        $role = $session->get('role');

        $user = new Utilisateur();
        $repository = $this->getDoctrine()->getManager()->getRepository('universalPharmatraitementBundle:Utilisateur');
        $user = $repository->findOneByidutilisateur($session->get('id'));

        $formUtilisateur = $this->createForm(UtilisateurType::class, $user);
        $formUtilisateurView = $formUtilisateur->createView();

        $formUtilisateur->handleRequest($request);

        if ($request->isMethod('POST') && $formUtilisateur->isValid()) {
//            dump($request);
//            exit();

            if ($formUtilisateur->get('Modifier')->isClicked()) {

                $this->getDoctrine()->getManager()->persist($user);
                $this->getDoctrine()->getManager()->flush();
//                dump($user);
//                exit();
            } else if ($formUtilisateur->get('MiseaJour')->isClicked()) {
                return $this->redirectToRoute('universal_pharmatraitement_mdp');
            }

            return $this->render('universalPharmatraitementBundle:frais:dashboard.html.twig', array('nom' => $nom, 'role' => $role));
        }

        return $this->render('universalPharmatraitementBundle:frais:cpuser.html.twig', array('nom' => $nom, 'role' => $role, 'formUtilisateur' => $formUtilisateurView, 'utilisateur' => $user));
    }

    /*
     * Controlleur qui permet de modifier les informations de l'utilisateur 
     * concernant son mot de passe
     */

    public function mdpAction(Request $request) {
        $session = $request->getSession();
        $nom = $session->get('nom');
        $role = $session->get('role');

        $user = new Utilisateur();

        $formMDP = $this->createForm(MotdePasseType::class, $user);
        $formMDPView = $formMDP->createView();

        $formMDP->handleRequest($request);

        if ($request->isMethod('POST') && $formMDP->isValid()) {
//            dump($request);
//            exit();
            $repository = $this->getDoctrine()->getManager()->getRepository('universalPharmatraitementBundle:Utilisateur');
            $user = $repository->findOneByidutilisateur($session->get('id'));

            $ancienMDP = sha1($formMDP['mdputilisateur']->getData());
//            dump($ancienMDP);
//            dump($user->getMdputilisateur());
//            exit();

            if ($formMDP->get('Modifier')->isClicked() && $ancienMDP == $user->getMdputilisateur()) {

                $nouveauMDP = sha1($formMDP['nouveaumdputilisateur']->getData());
//                dump($nouveauMDP);
//                exit();
                if ($ancienMDP != $nouveauMDP) {
                    $user->setMdputilisateur($nouveauMDP);
                    $this->getDoctrine()->getManager()->persist($user);
                    $this->getDoctrine()->getManager()->flush();
                } else {
                    $session->getFlashBag()->add('errMsg', 'L\'ancien mot de passe et le nouveau sont identiques. Merci de modifier');
                    return $this->render('universalPharmatraitementBundle:frais:mdp.html.twig', array('nom' => $nom, 'role' => $role, 'formMDP' => $formMDPView));
                }
            } elseif ($formMDP->get('Modifier')->isClicked() && $ancienMDP != $user->getMdputilisateur()) {

                $session->getFlashBag()->add('errMsg', 'Votre mot de passe actuel n\'est pas correct. Merci de modifier');
                return $this->render('universalPharmatraitementBundle:frais:mdp.html.twig', array('nom' => $nom, 'role' => $role, 'formMDP' => $formMDPView));
            }

            return $this->render('universalPharmatraitementBundle:frais:dashboard.html.twig', array('nom' => $nom, 'role' => $role));
        } elseif ($request->isMethod('POST') && !$formMDP->isValid()) {

            $session->getFlashBag()->add('errMsg', 'La confirmation n\'est pas identique au nouveau mot de passe. Merci de modifier');
            return $this->render('universalPharmatraitementBundle:frais:mdp.html.twig', array('nom' => $nom, 'role' => $role, 'formMDP' => $formMDPView));
        }

        return $this->render('universalPharmatraitementBundle:frais:mdp.html.twig', array('nom' => $nom, 'role' => $role, 'formMDP' => $formMDPView));
    }

    /*
     * Controlleur qui permet de lister les fiches de frais 
     * en fonction du nom du collaborateur et de l'état
     */
    public function fraisAction(Request $request) {
        $session = $request->getSession();
        $nom = $session->get('nom');
        $role = $session->get('role');

        $user = new Utilisateur();
        $user = $session->get('userfrais');
        $StatusDesFrais = new Statusdesfrais();
        $StatusDesFrais = $session->get('statusfrais');
        $tabFf = NULL;

        $formFicheFrais = $this->createForm(FichedefraisType::class);
        $formFicheFraisView = $formFicheFrais->createView();
        $formFicheFrais->handleRequest($request);
        //si le formulaire fiche de frais est soumis et valid
        if ($formFicheFrais->isSubmitted() && $formFicheFrais->isValid()) {
            $user = $formFicheFrais['utilisateur']->getData();
//            var_dump($user);
            //si l'utilisateur est différent de null
            if (!is_null($user))
                $userId = $user->getIdutilisateur();
            $StatusDesFrais = $formFicheFrais['etatfrais']->getData();
//            var_dump($StatusDesFrais);
//            exit();
            //si le status des frais est différent de null 
            if (!is_null($StatusDesFrais))
                $etatId = $StatusDesFrais->getIdstatfrais();

            $session->set('userfrais', $user);
            $session->set('statusfrais', $StatusDesFrais);
                
            //récupération des fiches de frais à traiter
            $repository = $this->getDoctrine()->getManager()->getRepository("universalPharmatraitementBundle:Fichedefrais");
            $tabFf = $repository->findBy(['idutilisateur' => $userId, 'idstatfrais' => $etatId], array('anneemoisligneff' => 'desc'));
//            var_dump($tabFf['1']);
//            exit();
        }
//        var_dump($tabFf);
//        exit();
        return $this->render('universalPharmatraitementBundle:frais:frais.html.twig', array('nom' => $nom, 'role' => $role,
                    'formFF' => $formFicheFraisView, 'tabFF' => $tabFf));
    }

    /*
     * Controlleur qui permet de gérer les fiches de frais 
     * en fonction du nom du collaborateur et de l'état
     */
    public function fraisidAction($id, $type, Request $request) {

        $ficheFrais = new Fichedefrais();
        $statusFrais = new Statusdesfrais();
        //récupération de la fiche de frais à traiter
        $repository = $this->getDoctrine()->getManager()->getRepository("universalPharmatraitementBundle:Fichedefrais");
        $ficheFrais = $repository->findOneByIdff($id);
//        var_dump($ficheFrais->getIdstatfrais()->getIdstatfrais());
//        exit();

        if (!is_null($ficheFrais) && ($ficheFrais->getIdstatfrais()->getIdstatfrais() == $type)) {
            switch ($type) {
                case "CL":
                    //rajouter un repository pour statusdesfrais pour récupérer la ligne correspondant à CL
                    $repositoryStatus = $this->getDoctrine()->getManager()->getRepository("universalPharmatraitementBundle:Statusdesfrais");
                    $statusFrais = $repositoryStatus->findOneByIdstatfrais("VA");
//                    var_dump($statusFrais);
//                    exit();
                    //puis le réintégrer dans fiche de frais
                    $ficheFrais->setIdstatfrais($statusFrais);
                    break;
                case "VA":
                    //rajouter un repository pour statusdesfrais pour récupérer la ligne correspondant à MP
                    $repositoryStatus = $this->getDoctrine()->getManager()->getRepository("universalPharmatraitementBundle:Statusdesfrais");
                    $statusFrais = $repositoryStatus->findOneByIdstatfrais("MP");
                    $ficheFrais->setIdstatfrais($statusFrais);
                    break;
                case "MP":
                    //rajouter un repository pour statusdesfrais pour récupérer la ligne correspondant à RB
                    $repositoryStatus = $this->getDoctrine()->getManager()->getRepository("universalPharmatraitementBundle:Statusdesfrais");
                    $statusFrais = $repositoryStatus->findOneByIdstatfrais("RB");
                    $ficheFrais->setIdstatfrais($statusFrais);
                    break;
            }
        }
        $this->getDoctrine()->getManager()->persist($ficheFrais);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('universal_pharmatraitement_frais');
    }

}
